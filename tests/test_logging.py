import unittest
import logging
from own.own_logging.logging import return_log_level

class Test_Own_CSV(unittest.TestCase):

    def test_return_log_level(self):
        self.assertEqual(logging.DEBUG, return_log_level("DEBUG"))
        self.assertEqual(logging.DEBUG, return_log_level("debug"))
        self.assertEqual(logging.DEBUG, return_log_level("Debug"))
        self.assertEqual(logging.DEBUG, return_log_level("DebUG"))
        self.assertEqual(logging.INFO, return_log_level("INFO"))
        self.assertEqual(logging.WARNING, return_log_level("WARNING"))
        self.assertEqual(logging.ERROR, return_log_level("ERROR"))
        self.assertEqual(logging.CRITICAL, return_log_level("CRITICAL"))
        self.assertEqual(logging.INFO, return_log_level("Blap"))       