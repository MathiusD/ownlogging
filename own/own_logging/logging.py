import logging

"""
    Fonction return_log_level
    %       Cette fonction returne un entier correspondant au niveau de
            logging souhaité, sinon si la chaine de caractère ne correspond
            à aucun format ou que l'argument n'est pas convertible en chaine
            de caractère on renvoie l'entier correspondant au niveau de
            logging "INFO".
    %IN     level : Chaine de caractère représentant le niveau de logging 
                    souhaité
    %OUT    log_level : Entier représentant le niveau de logging demandé
"""

def return_log_level(level):
    verif = False
    try:
        level = str(level).upper()
        verif = True
    finally:
        if verif == True:
            if level == "DEBUG":
                log_level = logging.DEBUG
            else:
                if level == "WARNING":
                    log_level = logging.WARNING
                else:
                    if level == "ERROR":
                        log_level = logging.ERROR
                    else:
                        if level == "CRITICAL":
                            log_level = logging.CRITICAL
                        else:
                            log_level = logging.INFO
        else:
            log_level = logging.INFO
        return log_level
