# Module own_logging

## Description

Ce module nous permet de renvoyer la variable de logging associé à un niveau souhaité.

### Fonctions

Dans ce module on possède qu'une fonction :

* return_log_level() qui nous renvoie la variable de log correspondant au niveau de logging souhaité.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_logging. On y teste notre fonction avec 9 données avec casse correcte ou non et avec des niveaux existants ou non.

### Dépendances

Ce module dépend du module constructeur logging.
